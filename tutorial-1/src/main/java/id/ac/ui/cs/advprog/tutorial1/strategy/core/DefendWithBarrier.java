package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    @Override
    public String defend() {
        return "Defend with Barrier : BA-DOOM";
    }

    @Override
    public String getType() {
        return "barrier";
    }
}
