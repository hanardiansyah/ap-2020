package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class ChaosUpgrade extends Weapon {

    Weapon weapon;

    public ChaosUpgrade(Weapon weapon) {

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    @Override
    public int getWeaponValue() {
        Random rand = new Random();
        int value_upgraded = rand.nextInt(6) + 50;
        return weapon.getWeaponValue() + value_upgraded;
    }

    @Override
    public String getDescription() {
        return weapon.getDescription() + " Chaos Upgrade";
    }
}

