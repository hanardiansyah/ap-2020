package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;

    public RawUpgrade(Weapon weapon) {

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    @Override
    public int getWeaponValue() {
        // TODO: Complete me
        Random rand = new Random();
        int value_upgraded = rand.nextInt(6) + 5;
        return weapon.getWeaponValue() + value_upgraded;
    }

    @Override
    public String getDescription() {
        return weapon.getDescription()  + " Raw Upgrade";
    }
}
