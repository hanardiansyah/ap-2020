package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Naruto", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        int prevSize = guildMaster.getChildMembers().size();
        Member dummy = new PremiumMember("Name", "Role");
        guild.addMember(guildMaster, dummy);
        assertEquals(guildMaster.getChildMembers().size(), prevSize + 1);
    }

    @Test
    public void testMethodRemoveMember() {
        Member dummy = new PremiumMember("Name", "Role");
        guild.addMember(guildMaster, dummy);
        int prevSize = guildMaster.getChildMembers().size();
        guild.removeMember(guildMaster, dummy);
        assertEquals(guildMaster.getChildMembers().size(), prevSize - 1);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Sasuke", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member dummy = new PremiumMember("Name", "Role");
        guild.addMember(guildMaster, dummy);
        assertEquals(dummy, guild.getMember("Name", "Role"));
    }
}
