package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Asuna", "Waifu");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Asuna", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Waifu", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing(){
        int prevSize = member.getChildMembers().size();
        Member dummy = new OrdinaryMember("Name", "Role");
        member.addChildMember(dummy);
        assertEquals(member.getChildMembers().size(), prevSize);
        member.removeChildMember(dummy);
        assertEquals(member.getChildMembers().size(), prevSize);
    }
}
