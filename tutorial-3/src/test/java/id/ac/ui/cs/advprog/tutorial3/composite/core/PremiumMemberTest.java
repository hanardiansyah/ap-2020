package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Aqua", "Goddess");
    }

    @Test
    public void testMethodGetName() {
        assertEquals("Aqua", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        int prevSize = member.getChildMembers().size();
        Member dummy = new PremiumMember("Name", "Role");
        member.addChildMember(dummy);
        assertEquals(member.getChildMembers().size(), prevSize + 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member dummy = new PremiumMember("Name", "Role");
        member.addChildMember(dummy);
        int prevSize = member.getChildMembers().size();
        member.removeChildMember(dummy);
        assertEquals(member.getChildMembers().size(), prevSize - 1);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member dummy1 = new PremiumMember("Name_1", "Role_1");
        Member dummy2 = new PremiumMember("Name_2", "Role_2");
        Member dummy3 = new PremiumMember("Name_3", "Role_3");
        member.addChildMember(dummy1);
        member.addChildMember(dummy2);
        member.addChildMember(dummy3);
        int prevSize = member.getChildMembers().size();
        Member dummy4 = new PremiumMember("Name_4", "Role_4");
        member.addChildMember(dummy4);
        assertEquals(member.getChildMembers().size(), prevSize);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        // TODO: Complete me
        Member guild_master = new PremiumMember("dummyMaster", "Master");
        Member dummy1 = new PremiumMember("Name_1", "Role_1");
        Member dummy2 = new PremiumMember("Name_2", "Role_2");
        Member dummy3 = new PremiumMember("Name_3", "Role_3");
        guild_master.addChildMember(dummy1);
        guild_master.addChildMember(dummy2);
        guild_master.addChildMember(dummy3);
        int prevSize = guild_master.getChildMembers().size();
        Member dummy4 = new PremiumMember("Name_4", "Role_4");
        guild_master.addChildMember(dummy4);
        assertEquals(guild_master.getChildMembers().size(), prevSize + 1);
    }
}
