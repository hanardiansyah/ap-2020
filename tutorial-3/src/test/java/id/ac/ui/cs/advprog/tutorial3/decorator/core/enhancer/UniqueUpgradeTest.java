package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp() {
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName() {
        assertEquals("Shield", uniqueUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription() {
        assertTrue(uniqueUpgrade.getDescription().substring(uniqueUpgrade.getDescription().length() - 14)
                .equals("Unique Upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue() {
        boolean dummy;
        int weaponValue = uniqueUpgrade.getWeaponValue();
        dummy = (weaponValue == uniqueUpgrade.weapon.getWeaponValue() + 10);
        dummy = dummy || (weaponValue == uniqueUpgrade.weapon.getWeaponValue() + 11);
        dummy = dummy || (weaponValue == uniqueUpgrade.weapon.getWeaponValue() + 12);
        dummy = dummy || (weaponValue == uniqueUpgrade.weapon.getWeaponValue() + 13);
        dummy = dummy || (weaponValue == uniqueUpgrade.weapon.getWeaponValue() + 14);
        dummy = dummy || (weaponValue == uniqueUpgrade.weapon.getWeaponValue() + 15);
        assertTrue(dummy);
    }
}
