package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class MajesticKnight extends Knight {

    public MajesticKnight(Armory armory) {
        this.armory = armory;
        this.name = "Majestic";
    }

    @Override
    public void prepare() {
        this.armor = this.armory.craftArmor();
        this.weapon = this.armory.craftWeapon();
    }

    public String getDescriptions() {
        return "Majestic Knight";
    }
}
