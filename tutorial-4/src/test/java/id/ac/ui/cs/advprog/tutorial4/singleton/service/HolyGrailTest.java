package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    @Mock
    private HolyGrail holyGrail;

    public void testGetAWish() {
        assertTrue(holyGrail.getHolyWish() instanceof HolyWish);
    }
}
